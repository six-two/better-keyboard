from key_event import TimeStamp, InputEvent, EV_KEY, EK_PRESSED, EK_RELEASED, send_event_and_sync, send_key
from typing import Optional
import sys
from key_codes import get_key_name


class KeyOutput:
    def __init__(self):
        self.pressed_keys: set[int] = set()

    def press_key_combo(self, modifiers: set[int], key: Optional[int], time: TimeStamp):
        to_release = self.pressed_keys.difference(modifiers)
        to_press = modifiers.difference(self.pressed_keys)
        time = time.copy()

        for k in to_release:
            self.release_key(k, time)
            time.add_millis(10)

        for k in to_press:
            self.press_key(k, time)
            time.add_millis(10)

        self.pressed_keys = modifiers

        if key is not None:
            send_key(time, key)

    def is_pressed(self, code: int) -> bool:
        return code in self.pressed_keys

    def release_key(self, code: int, time: TimeStamp):
        if code in self.pressed_keys:
            self.pressed_keys.remove(code)
            event = InputEvent(time, EV_KEY, code, EK_RELEASED)
            send_event_and_sync(event)
        else:
            print("Already released:", get_key_name(code), file=sys.stderr)

    def press_key(self, code: int, time: TimeStamp):
        if code not in self.pressed_keys:
            self.pressed_keys.add(code)
            event = InputEvent(time, EV_KEY, code, EK_PRESSED)
            send_event_and_sync(event)
        else:
            print("Already pressed:", get_key_name(code), file=sys.stderr)

