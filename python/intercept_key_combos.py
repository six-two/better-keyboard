#!/usr/bin/env python3

# pylint: disable=unused-wildcard-import
import os
import sys
import time
import traceback
from typing import Optional, NamedTuple
import yaml
# local files
from key_codes import KEY, load_key_name_definitions, get_key_name
from key_event import *
from key_output import KeyOutput
from key_tracker import HardwareKeyTracker


"""
Configure this script via ./config.yaml
Run it like this: ./run.sh
"""

KEY_MAGIC = -1
ALLOWED_TYPE_TIME = 0.2 #seconds
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

class Config(NamedTuple):
    normal_map: dict[int, int]
    magic_map: dict[int, list[int]]
    ignore_keys: set[int]


def parse_config(path: str) -> Config:
    load_key_name_definitions()
    KEY["MAGIC"] = KEY_MAGIC

    with open(path, "r") as f:
        config = yaml.safe_load(f)

    normal_map = {}
    for key_from, key_to in config.get("normal_map", {}).items():
        normal_map[KEY[key_from]] = KEY[key_to]

    magic_map = {}
    for key_from, key_to in config.get("magic_map", {}).items():
        magic_map[KEY[key_from]] = [KEY[x] for x in key_to]   

    ignore_keys = set([KEY[x] for x in config.get("ignore_keys", [])])

    return Config(normal_map, magic_map, ignore_keys)


class ModifierIntercept:
    def __init__(self):
        self.hardware_keys = HardwareKeyTracker()
        self.output_keys = KeyOutput()

        config_path = os.path.join(SCRIPT_DIR, "key_combos.yaml")
        self.config = parse_config(config_path)

        self.modifiers = set([KEY_MAGIC])
        for location in ["RIGHT", "LEFT"]:
            for modifier_type in ["SHIFT", "CTRL", "META", "ALT"]:
                code = KEY[location + modifier_type]
                self.modifiers.add(code)

    def run(self):
        while True:
            group = self.read_event_group()

            drop_group = False
            for event in group:
                if event.event_type == EV_KEY:
                    self.handle_key_event(event)
                    drop_group = True

            if not drop_group:
                for event in group:
                    write_input_event(event)
                flush_input_events()

    def read_event_group(self) -> list[InputEvent]:
        group = []
        while True:
            event = read_input_event()
            group.append(event)
            # Close the current group, whenever you get a SYNC packet
            if event.event_type == EV_SYN:
                return group

    def handle_key_event(self, event: InputEvent) -> None:
        try:
            if event.code in self.config.ignore_keys:
                # Just forward the keys
                if event.value == EK_PRESSED:
                    self.output_keys.press_key(event.code, event.time)
                elif event.value == EK_RELEASED:
                    self.output_keys.release_key(event.code, event.time)
            else:
                # Do the remapping magic
                if event.value == EK_RELEASED:
                    modifiers, code = self.get_key_combo(event.code, event.time)
                    self.output_keys.press_key_combo(modifiers, code, event.time)

            self.hardware_keys.on_event(event)
        except Exception:
            traceback.print_exc(file=sys.stderr)

    def get_key_combo(self, released_code: int, time: TimeStamp) -> tuple[set[int], Optional[int]]:
        modifiers = set()
        pressed_keys = self.hardware_keys.get_pressed_keys()
        for code in pressed_keys:
            if KEY_MAGIC in modifiers:
                remapped_codes = self.config.magic_map.get(code, [code])
                remapped_code = None
                if type(remapped_codes) == list:
                    for c in remapped_codes:
                        if c in self.modifiers:
                            modifiers.add(c)
                        else:
                            if remapped_code:
                                print("Multiple normal keys in key-combo", file=sys.stderr)
                            remapped_code = c
                else:
                    remapped_code = remapped_codes
            else:
                remapped_code = self.config.normal_map.get(code, code)

            if code == released_code:
                # Don't type the magic key
                if KEY_MAGIC in modifiers:
                    modifiers.remove(KEY_MAGIC)

                if remapped_code in self.modifiers:
                    if code in self.modifiers or self.output_keys.is_pressed(released_code) or self.hardware_keys.get_pressed_time(code, time) > ALLOWED_TYPE_TIME:
                        # the key was not used as normal key, but as a modifier
                        return (modifiers, None)
                    else:
                        return (modifiers, code)
                return (modifiers, remapped_code)

            if remapped_code in self.modifiers:
                modifiers.add(remapped_code)

        print(f"Key {get_key_name(released_code)} not in list of pressed keys", file=sys.stderr)


if __name__ == "__main__":
    try:
        mi = ModifierIntercept()
        mi.run()
    except KeyboardInterrupt:
        pass
