#!/usr/bin/env python3
# pylint: disable=unused-wildcard-import
import sys
# local files
from key_event import *
from key_codes import load_key_name_definitions, get_key_name

def main(verbose: bool, forward: bool):
    load_key_name_definitions()
    print_stream = sys.stderr if forward else sys.stdout

    while True:
        event = read_input_event()
        if forward:
            write_input_event(event)
            flush_input_events()

        if event.event_type == EV_SYN:
            verbose and print("EV_SYN", event, file=print_stream)
        elif event.event_type == EV_KEY:
            key_name = get_key_name(event.code)
            if event.value == EK_RELEASED:
                print(key_name, "released", file=print_stream)
            elif event.value == EK_PRESSED:
                print(key_name, "pressed", file=print_stream)
            elif event.value == EK_REPEATED:
                verbose and print(key_name, "repeated", file=print_stream)
            else:
                print("EV_KEY", event, file=print_stream)
        elif event.event_type == EV_MSC:
            verbose and print("EV_MSC", event, file=print_stream)
        else:
            print("Unknown", event, file=print_stream)


if __name__ == "__main__":
    try:
        # Display all key events, not just short parsed versions
        verbose = "-v" in sys.argv
        # Forward all events to stdout, so that you can debug other programs
        forward = "-f" in sys.argv

        main(verbose, forward)
    except KeyboardInterrupt:
        print("[KeyboardInterrupt] Exiting...", file=sys.stderr)
