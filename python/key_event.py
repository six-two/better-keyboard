import sys
import time
import struct
from typing import NamedTuple

"""
Requires https://gitlab.com/interception/linux/tools

Example invocation:
intercept /dev/input/event2 | ~/c/better-keyboard/dump.py
"""

# Keycodes are defined in /usr/include/linux/input-event-codes.h
# API documentation can be found at https://www.kernel.org/doc/html/v4.17/input/event-codes.html

# Event types
## Used as markers to separate events. Events may be separated in time or in space, such as with the multitouch protocol
EV_SYN = 0x00
## Used to describe state changes of keyboards, buttons, or other key-like devices.
EV_KEY = 0x01
## Used to describe miscellaneous input data that do not fit into other types
EV_MSC = 0x04

# EV_KEY values
## When the key is released, an event is emitted with value 0
EK_RELEASED = 0
## When a key is depressed, an event with the key’s code is emitted with value 1
EK_PRESSED = 1
## Some hardware send events when a key is repeated. These events have a value of 2
EK_REPEATED = 2

_INPUT_EVENT_SIZE = 8 + 8 + 2 + 2 + 4
_INPUT_EVENT_FORMAT = "=QQhhi"


class TimeStamp:
    SECOND = 1000 * 1000

    def __init__(self, seconds: int, micros: int):
        self.seconds = seconds
        self.micros = micros

    def add_millis(self, millis: int) -> None:
        self.micros = self.micros + 1000 * millis
        # handle overflows
        if self.micros >= TimeStamp.SECOND:
            self.seconds += int(self.micros / TimeStamp.SECOND)
            self.micros = self.micros % TimeStamp.SECOND

    def subtract(self, time):
        self.seconds -= time.seconds
        self.micros -= time.micros

        # handle underflow, inefficient but simple
        while self.micros < 0:
            self.micros += TimeStamp.SECOND
            self.seconds -= 1

    def to_seconds(self) -> float:
        return self.seconds + (self.micros / TimeStamp.SECOND)

    def copy(self):
        return TimeStamp(self.seconds, self.micros)

    def __str__(self) -> str:
        return str(self.to_seconds())

    def __repr__(self) -> str:
        return str(self)


class InputEvent(NamedTuple):
    time: TimeStamp # 16 bytes
    event_type: int # 2 bytes
    code: int # 2 bytes
    value: int # 4 bytes


def send_event_and_sync(event: InputEvent):
    write_input_event(event)
    write_input_event(InputEvent(event.time, EV_SYN, 0, 0))
    flush_input_events()


def send_key(time: TimeStamp, code: int):
    write_input_event(InputEvent(time, EV_KEY, code, EK_PRESSED))
    write_input_event(InputEvent(time, EV_SYN, 0, 0))
    time_release = time.copy()
    time_release.add_millis(20)
    write_input_event(InputEvent(time_release, EV_KEY, code, EK_RELEASED))
    write_input_event(InputEvent(time_release, EV_SYN, 0, 0))
    flush_input_events()

def read_input_event(byte_stream = sys.stdin.buffer) -> InputEvent:
    # Make sure we read the correct number of bytes
    input_event_bytes = b""
    while len(input_event_bytes) < _INPUT_EVENT_SIZE:
        missing = _INPUT_EVENT_SIZE - len(input_event_bytes)
        read = byte_stream.read(missing)
        if not read:
            raise Exception("End of stream or nonblocking read method")
        input_event_bytes += read

    if len(input_event_bytes) != _INPUT_EVENT_SIZE:
        raise Exception(f"Expected to read {_INPUT_EVENT_SIZE} bytes, but got {len(input_event_bytes)}")
    
    # Unpack the event into our named tuple
    unpacked = struct.unpack(_INPUT_EVENT_FORMAT, input_event_bytes)
    time = TimeStamp(*unpacked[:2])
    return InputEvent(time, *unpacked[2:])

def write_input_event(event: InputEvent, byte_stream = sys.stdout.buffer) -> None:
    unpacked = [event.time.seconds, event.time.micros, *event[1:]]
    packed = struct.pack(_INPUT_EVENT_FORMAT, *unpacked)
    byte_stream.write(packed)


def flush_input_events(byte_stream = sys.stdout.buffer) -> None:
    byte_stream.flush()

