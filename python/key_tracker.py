import sys
# local files
from key_event import InputEvent, TimeStamp, send_event_and_sync, EK_PRESSED, EK_RELEASED, EV_KEY


class HardwareKeyTracker:
    def __init__(self):
        self.pressed_time: dict[int, float] = {}

    def on_event(self, event: InputEvent) -> None:
        if event.event_type == EV_KEY:
            if event.value == EK_PRESSED:
                self.on_press(event.code, event.time)
            elif event.value == EK_RELEASED:
                self.on_release(event.code)

    def on_press(self, code: int, time: TimeStamp) -> None:
        self.pressed_time[code] = time.to_seconds()
    
    def on_release(self, code: int) -> None:
        try:
            self.pressed_time.pop(code)
        except KeyError:
            pass # key was not pressed

    def is_pressed(self, code: int) -> bool:
        return code in self.pressed_time

    def get_pressed_keys(self) -> list[int]:
        """
        Returns the currently pressed keys sorted by time
        """
        sorted_codes = sorted(self.pressed_time.items(), key=lambda x: x[1])
        # print(sorted_codes, file=sys.stderr)
        return [code for code, time in sorted_codes]

    def get_pressed_time(self, code: int, now: TimeStamp) -> float:
        start = self.pressed_time.get(code)
        if start is None:
            # The key is not pressed, so it is zero seconds
            return 0
        else:
            return now.to_seconds() - start
