import re

# Maps key codes to names
KEY_NAMES: dict[int, str] = {}
# Maps key names to codes
KEY: dict[str, int] = {}


def get_key_name(code: int) -> str:
    return KEY_NAMES.get(code, str(code))

def load_key_name_definitions(path: str = "/usr/include/linux/input-event-codes.h"):
    if not KEY_NAMES:
        REGEX_KEY = re.compile(r"^#define\s+(KEY_\w+)\s+(0x[0-9a-fA-F]+|[0-9]+)")
        
        with open(path, "r") as f:
            text = f.read()

        for line in text.split("\n"):
            match = REGEX_KEY.match(line)
            if match:
                name, code = match.groups()
                short_name = name[4:] # remove the "KEY_" prefix
                parsed_code = int(code, 16) if code.startswith("0x") else int(code)
                # print(name, code, parsed_code)
                KEY_NAMES[parsed_code] = name
                KEY[short_name] = parsed_code


if __name__ == "__main__":
    load_key_name_definitions()
    print(KEY)
    print(f"Loaded {len(KEY)} key(s)")
