#!/usr/bin/bash
KEYBOARD="$1"
SCRIPT_DIR=`dirname "${BASH_SOURCE[0]}"`
TYPE="release" # "debug"

if [[ -n "${KEYBOARD}" ]] && [ -e "${KEYBOARD}" ]; then
  # Make sure we have root rights
  sudo echo "Intercepting ${KEYBOARD}, press Ctrl-C to stop"
  # Wait for the enter key to be released
  sleep 0.1

  # grab the device | modify the keys | create a virtual keyboard
  sudo intercept -g "${KEYBOARD}" | "${SCRIPT_DIR}/rust/target/${TYPE}/keycode-remapper" | sudo uinput -d "${KEYBOARD}"
else
  echo "Usage: $0 <device_file>"
  echo "Example: $0 /dev/input/event2"
  echo
  echo "Error: Keyboard device file '${KEYBOARD}' does not exist!"
  exit 1
fi
