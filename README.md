# better-keyboard

A keyboard remapper based on [interception tools](https://gitlab.com/interception/linux/tools).
I first wrote it in Python, and then rewrote it in Rust.
The Rust version has more features, the development on the Python version has stopped.

## Installation

First copy over the default config and make any changes you want:
```
mkdir -p ~/.config/better-keyboard/
cp ./config.yaml ~/.config/better-keyboard/config.yaml
```

Then build the binary:
```
cd rust
cargo build --release
```

### Autostart

First copy the binary to a dir in the path:
```
sudo cp ./target/release/keycode-remapper /usr/bin
```

Then create a `/etc/interception/udevmon.d/better-keyboard.yaml` with the following content:
```
# Run as user to read that users config file. Also decreases the attack surface
# Only apply the changes to external keyboards, so that I can use the internal keyboard if something goes wrong
- JOB: intercept -g $DEVNODE | sudo --user=<YOUR_USER_NAME> /usr/bin/keycode-remapper | uinput -d $DEVNODE
  DEVICE:
    EVENTS:
      EV_KEY: [[KEY_CAPSLOCK, KEY_ESC]]
    LINK: /dev/input/by-id/usb-.*-event-kbd
```

Finally start and enable `udevmon`:
```
sudo systemctl enable --now udevmon
```

## TODO

- support for pressed keys (like arrow keys)
- add mapping for typed keys (like shift -> delete)
- think of something good for caps lock (maybe Caps -> Esc, Magic+Caps -> toggle Caps lock)
- Record and replay macros (like passwords, etc)
