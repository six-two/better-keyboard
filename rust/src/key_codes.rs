use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs;

/**
 * Can contvert key names to and from key codes. Removes the leading "KEY_" form key names, so "KEY_TAB" becomes "TAB"
 */
pub struct KeyNameMap {
    name_to_code: HashMap<String, i16>,
    code_to_name: HashMap<i16, String>,
    modifiers: HashSet<i16>,
    modifier_regex: Regex,
}

pub const KEY_MAGIC: i16 = -1;

impl KeyNameMap {
    fn new() -> Self {
        return KeyNameMap {
            name_to_code: HashMap::new(),
            code_to_name: HashMap::new(),
            modifiers: HashSet::new(),
            modifier_regex: Regex::new("^(LEFT|RIGHT)?(ALT|CTRL|MAGIC|META|SHIFT)$").unwrap(),
        };
    }

    fn insert_key(self: &mut Self, key_name: &str, key_code: i16) {
        let short_key_name = match key_name.strip_prefix("KEY_") {
            Some(x) => x,
            None => key_name,
        };
        if self
            .name_to_code
            .insert(short_key_name.to_owned(), key_code)
            .is_some()
        {
            panic!("Duplicate key name: {}", key_name);
        }
        if self
            .code_to_name
            .insert(key_code, short_key_name.to_owned())
            .is_some()
        {
            panic!("Duplicate key code: {}", key_code);
        }

        if self.modifier_regex.is_match(short_key_name) {
            self.modifiers.insert(key_code);
        }
    }

    // pub fn len(self: &Self) -> usize {
    //     self.name_to_code.len()
    // }

    pub fn get_key_name(self: &Self, key_code: i16) -> Result<&str, String> {
        match self.code_to_name.get(&key_code) {
            Some(x) => Ok(x),
            None => Err(format!("Unknown key code: {}", key_code)),
        }
    }

    pub fn get_key_code(self: &Self, key_name: &str) -> Result<i16, String> {
        match self.name_to_code.get(key_name) {
            Some(x) => Ok(*x),
            None => Err(format!("Unknown key name: {}", key_name)),
        }
    }

    pub fn is_modifier(self: &Self, key_code: i16) -> bool {
        self.modifiers.contains(&key_code)
    }
}

pub fn load_key_codes() -> KeyNameMap {
    let mut key_map = KeyNameMap::new();
    key_map.insert_key("KEY_MAGIC", KEY_MAGIC);

    let filename = "/usr/include/linux/input-event-codes.h";
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let re = Regex::new(r"^#define\s+(KEY_\w+)\s+(0x[0-9a-fA-F]+|[0-9]+)").unwrap();
    for line in contents.split("\n") {
        match re.captures(line) {
            Some(x) => {
                let name = x.get(1).unwrap().as_str();
                let code_str = x.get(2).unwrap().as_str();
                let code_parse_result;
                if code_str.starts_with("0x") {
                    code_parse_result = i16::from_str_radix(&code_str[2..], 16);
                } else {
                    code_parse_result = i16::from_str_radix(code_str, 10);
                }
                let code = code_parse_result.expect("Integer parsing failed");
                key_map.insert_key(name, code)
            }
            None => {}
        }
    }

    return key_map;
}
