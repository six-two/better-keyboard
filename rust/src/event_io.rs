use byteorder::{NativeEndian, ReadBytesExt, WriteBytesExt};
use std::io;
use std::io::{Read, Write};
use crate::key_event::{EV_SYN, Timestamp, KeyEvent};

fn e2s(error: io::Error) -> String {
    format!("IO-Error: {:?}", error)
}

pub struct EventWriter {
    output_stream: Box<dyn Write>,
}

impl EventWriter {
    pub fn from_stdout() -> Self {
        let stdout = io::stdout();
        EventWriter {
            output_stream: Box::new(stdout),
        }
    }

    // pub fn write_key(self: &mut Self, time: &Timestamp, code: i16, pressed: bool) -> Result<(), String> {
    //     let value = if pressed {EK_PRESSED} else {EK_RELEASED};
    //     self.internal_write(&time, EV_KEY, code, value)
    // }

    pub fn write_event(self: &mut Self, event: &KeyEvent) -> Result<(), String> {
        self.internal_write(&event.time, event.event_type, event.code, event.value)
    }
    
    pub fn write_event_and_sync(self: &mut Self, event: &KeyEvent) -> Result<(), String> {
        self.write_event(event)?;
        self.internal_write(&event.time, EV_SYN, 0, 0)?;
        self.flush()
    }
    
    fn internal_write(self: &mut Self, time: &Timestamp, event_type: i16, code: i16, value: i32) -> Result<(), String> {
        let stream = self.output_stream.as_mut();

        stream.write_i64::<NativeEndian>(time.get_seconds()).map_err(e2s)?;
        stream.write_i64::<NativeEndian>(time.get_micros()).map_err(e2s)?;
        stream.write_i16::<NativeEndian>(event_type).map_err(e2s)?;
        stream.write_i16::<NativeEndian>(code).map_err(e2s)?;
        stream.write_i32::<NativeEndian>(value).map_err(e2s)?;

        Ok(())
    }

    pub fn flush(self: &mut Self) -> Result<(), String> {
        self.output_stream.flush().map_err(e2s)
    }
}


pub struct EventReader {
    input_stream: Box<dyn Read>,
}

impl EventReader {
    pub fn from_stdin() -> Self {
        let stdin = io::stdin();
        let input_stream = Box::new(stdin);
        EventReader {
            input_stream,
        }
    }

    pub fn read_event_group(self: &mut Self) -> Result<Vec<KeyEvent>, String> {
        let mut group = Vec::new();
        loop {
            let event = self.read_event()?;
            let event_type = event.event_type;
            group.push(event);

            if event_type == EV_SYN {
                return Ok(group)
            }
        }
    }

    pub fn read_event(self: &mut Self) -> Result<KeyEvent, String> {
        let stream = self.input_stream.as_mut();

        let seconds = stream.read_i64::<NativeEndian>().map_err(e2s)?;
        let micros = stream.read_i64::<NativeEndian>().map_err(e2s)?;
        let event_type = stream.read_i16::<NativeEndian>().map_err(e2s)?;
        let code = stream.read_i16::<NativeEndian>().map_err(e2s)?;
        let value = stream.read_i32::<NativeEndian>().map_err(e2s)?;

        let time = Timestamp::new(seconds, micros).unwrap();
        Ok(KeyEvent {
            time,
            event_type,
            code,
            value,
        })
    }
}

