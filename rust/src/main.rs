mod background_checker;
mod config_parser;
mod event_io;
mod key_codes;
mod key_event;
mod key_input;
mod key_output;

use background_checker::EnabledToggler;
use config_parser::Config;
use event_io::{EventReader, EventWriter};
use key_codes::{load_key_codes, KeyNameMap, KEY_MAGIC};
use key_event::{KeyEvent, Timestamp, EK_PRESSED, EK_RELEASED, EV_KEY};
use key_input::InputKeyTracker;
use key_output::KeyOutput;

use dirs;
use std::collections::HashSet;

// debug: sudo intercept /dev/input/event2 | ~/c/better-keyboard/keycode-remapper/target/debug/keycode-remapper | ~/c/better-keyboard/dump.py
// test: sudo intercept -g $KEYBOARD | ~/c/better-keyboard/keycode-remapper/target/debug/keycode-remapper | sudo uinput -d $KEYBOARD

const MAX_TYPE_SECONDS: f64 = 0.2;

struct BetterKeyboard {
    key_names: KeyNameMap,
    config: Config,
    event_reader: EventReader,
    input: InputKeyTracker,
    output: KeyOutput,
    enabled_checker: EnabledToggler,
}

impl BetterKeyboard {
    fn init() -> Result<Self, String> {
        let key_names = load_key_codes();

        let os_config_dir = dirs::config_dir().ok_or("Can not find config directory")?;
        let config_path = os_config_dir.join("better-keyboard").join("config.yaml");
        let config_path_string = config_path
            .to_str()
            .ok_or("Config file path is not valid UTF-8")?;
        eprintln!("Loading config file: {}", config_path_string);
        let config = config_parser::parse_file(config_path_string, &key_names)?;

        let event_reader = EventReader::from_stdin();
        let input = InputKeyTracker::new();
        let event_writer = EventWriter::from_stdout();
        let output = KeyOutput::new(event_writer);
        let enabled_checker = EnabledToggler::new();

        Ok(BetterKeyboard {
            key_names,
            config,
            event_reader,
            input,
            output,
            enabled_checker,
        })
    }

    fn run(self: &mut Self) -> Result<(), String> {
        loop {
            let mut forward_group = true;
            let group = self.event_reader.read_event_group()?;
            for event in &group {
                if event.event_type == EV_KEY {
                    self.enabled_checker.update(&self.config, &event);
                    forward_group = self.handle_key_event(&event)?;
                }
            }
            if forward_group {
                self.output.forward_events(&group)?;
            }
        }
    }

    fn handle_key_event(self: &mut Self, event: &KeyEvent) -> Result<bool, String> {
        let intercept_event =
            self.enabled_checker.is_enabled() && !self.config.is_key_ignored(event.code);
        if intercept_event {
            if event.value == EK_PRESSED {
                // do nothing
            } else if event.value == EK_RELEASED {
                // And here the magic happens
                self.handle_key_release(event)?;
            }
        } else {
            // Pass them through, but also update the internal state
            if event.value == EK_PRESSED {
                self.output.press_key(event.code, &event.time)?;
            } else if event.value == EK_RELEASED {
                self.output.release_key(event.code, &event.time)?;
            }
        }
        self.input.on_event(&event);

        Ok(!intercept_event)
    }

    fn handle_key_release(self: &mut Self, event: &KeyEvent) -> Result<(), String> {
        let mut modifiers: HashSet<i16> = HashSet::new();

        for (code, info) in self.input.get_pressed_keys() {
            let is_last = code == event.code;
            let is_magic = modifiers.contains(&KEY_MAGIC);

            let is_typed = if is_last {
                let original_key_is_modifier = self.key_names.is_modifier(code);
                let key_is_pressed = self.output.is_key_pressed(code);
                let pressed_to_long =
                    Timestamp::time_diff(&event.time, &info.time) > MAX_TYPE_SECONDS;

                !(original_key_is_modifier || key_is_pressed || pressed_to_long)
            } else {
                false
            };

            let mut remapped_code: Option<i16> = None;
            if !is_typed {
                remapped_code = Some(self.config.map_key_hold(code, is_magic));
            } else {
                let remapped_code_list = self.config.map_key_type(code, is_magic);
                for c in remapped_code_list {
                    if self.key_names.is_modifier(c) {
                        modifiers.insert(c);
                    } else {
                        if remapped_code.is_some() {
                            eprintln!("Multiple normal keys in key-combo");
                        }
                        remapped_code = Some(c);
                    }
                }
            }

            if let Some(remapped) = remapped_code {
                if is_last {
                    // handle the magic key
                    if remapped_code == Some(KEY_MAGIC) {
                        remapped_code = None
                    }
                    modifiers.remove(&KEY_MAGIC);
                    self.output
                        .press_key_combo(&modifiers, remapped_code, &event.time)?;
                    return Ok(());
                } else {
                    if self.key_names.is_modifier(remapped) {
                        modifiers.insert(remapped);
                    }
                }
            } else {
                eprintln!("No normal key in combination");
            }
        }

        eprintln!(
            "Key {} not found in the list of currently pressed keys",
            self.key_names.get_key_name(event.code)?
        );
        Ok(())
    }
}

fn main() {
    match BetterKeyboard::init() {
        Ok(mut bk) => match bk.run() {
            Ok(_) => {}
            Err(error) => {
                eprintln!("Runtime error: {}", error)
            }
        },
        Err(error) => {
            eprintln!("Error during initialisation: {}", error);
        }
    }
}
