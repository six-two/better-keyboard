use crate::key_event::{KeyEvent, Timestamp, EK_PRESSED, EK_RELEASED, EV_KEY};
use std::collections::HashMap;


#[derive(Clone, Debug)]
pub struct PressedInfo {
    pub time: Timestamp,
    pub used_as_modifier: bool,
}

pub struct InputKeyTracker {
    pressed_info: HashMap<i16, PressedInfo>,
}

impl InputKeyTracker {
    pub fn new() -> Self {
        InputKeyTracker {
            pressed_info: HashMap::new(),
        }
    }

    pub fn on_event(self: &mut Self, event: &KeyEvent) {
        if event.event_type == EV_KEY {
            match event.value {
                EK_PRESSED => {
                    let info = PressedInfo {
                        time: event.time.clone(),
                        used_as_modifier: false,
                    };
                    self.pressed_info.insert(event.code, info);
                }
                EK_RELEASED => {
                    self.pressed_info.remove(&event.code);
                }
                _ => {}
            }
        }
    }

    // pub fn get_pressed_key(self: &Self, code: i16) -> Option<PressedInfo> {
    //     self.pressed_info.get(&code).map(|x| x.clone())
    // }

    pub fn get_pressed_keys(self: &Self) -> Vec<(i16, &PressedInfo)> {
        let mut list: Vec<(i16, &PressedInfo)> = self
            .pressed_info
            .iter()
            .map(|(k, v)| (k.clone(), v))
            .collect();
        
        list.sort_by(|a, b| {
            let a_time = &a.1.time;
            let b_time = &b.1.time;
    
            a_time.cmp(b_time)
        });

        list
    }
}
