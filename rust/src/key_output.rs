use crate::key_event::{KeyEvent, Timestamp, EK_PRESSED, EK_RELEASED, EV_KEY};
use crate::event_io::EventWriter;
use std::collections::HashSet;

const TIME_BETWEEN_EVENTS: i64 = 10;

pub struct KeyOutput {
    pressed_keys: HashSet<i16>,
    writer: EventWriter,
}

impl KeyOutput {
    pub fn new(writer: EventWriter) -> Self {
        KeyOutput {
            pressed_keys: HashSet::new(),
            writer,
        }
    }

    pub fn press_key_combo(
        self: &mut Self,
        modifiers: &HashSet<i16>,
        code: Option<i16>,
        start_time: &Timestamp,
    ) -> Result<(), String> {
        let to_release = self.pressed_keys.difference(modifiers);
        let to_release: Vec<i16> = to_release.map(|x| *x).collect();
        let to_press = modifiers.difference(&self.pressed_keys);
        let to_press: Vec<i16> = to_press.map(|x| *x).collect();
        let mut time = start_time.clone();

        // release old modifiers
        for key in to_release {
            if self.release_key(key, &time)? {
                time.add_millis(TIME_BETWEEN_EVENTS);
            }
        }

        // press new modifiers
        for key in to_press {
            if self.press_key(key, &time)? {
                time.add_millis(TIME_BETWEEN_EVENTS);
            }
        }

        // Press the key if it was given
        if let Some(code) = code {
            // release key if it is already pressed
            if self.release_key(code, &time)? {
                time.add_millis(TIME_BETWEEN_EVENTS);
            }
            // press the key
            self.press_key(code, &time)?;
            time.add_millis(TIME_BETWEEN_EVENTS);
            self.release_key(code, &time)?;
        }
        Ok(())
    }

    pub fn is_key_pressed(self: &Self, code: i16) -> bool {
        self.pressed_keys.contains(&code)
    }

    pub fn press_key(self: &mut Self, code: i16, time: &Timestamp) -> Result<bool, String> {
        let change = !self.is_key_pressed(code);
        if change {
            self.pressed_keys.insert(code);
            let event = KeyEvent {
                time: time.clone(),
                event_type: EV_KEY,
                code,
                value: EK_PRESSED,
            };
            self.writer.write_event_and_sync(&event)?;
        } else {
            eprintln!("Already pressed: {}", code);
        }
        Ok(change)
    }

    pub fn release_key(self: &mut Self, code: i16, time: &Timestamp) -> Result<bool, String> {
        let change = self.is_key_pressed(code);
        if change {
            self.pressed_keys.remove(&code);
            let event = KeyEvent {
                time: time.clone(),
                event_type: EV_KEY,
                code,
                value: EK_RELEASED,
            };
            self.writer.write_event_and_sync(&event)?;
        } else {
            eprintln!("Already released: {}", code);
        }
        Ok(change)
    }

    pub fn forward_events(self: &mut Self, events: &Vec<KeyEvent>) -> Result<(), String> {
        for event in events {
            self.writer.write_event(event)?;
        }
        Ok(())
    }
}
