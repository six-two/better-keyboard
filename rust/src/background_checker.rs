use crate::config_parser::Config;
use crate::key_event::{KeyEvent, EK_PRESSED, EK_RELEASED, EV_KEY};

// How often to press the toggle enabled key in a row, before toggeling the key substitution
const TOGGLE_ENABLED_KEY_PRESS_COUNT: u8 = 4;

pub struct EnabledToggler {
    toggle_key_counter: u8,
    enabled: bool,
}

impl EnabledToggler {
    pub fn new() -> Self {
        EnabledToggler {
            toggle_key_counter: 0,
            enabled: true,
        }
    }

    pub fn update(self: &mut Self, config: &Config, event: &KeyEvent) {
        // Check for the toggling key being pressed
        if event.event_type == EV_KEY && (event.value == EK_PRESSED || event.value == EK_RELEASED) {
            if config.is_toggle_enabled_key(event.code) {
                // only count key presses
                if event.value == EK_PRESSED {
                    self.toggle_key_counter += 1;
                    eprintln!("Counter: {}", self.toggle_key_counter);
                    if self.toggle_key_counter >= TOGGLE_ENABLED_KEY_PRESS_COUNT {
                        self.enabled = !self.enabled;
                        eprintln!("Toggled the keyboard intercept. Enabled: {}", self.enabled);
                        self.toggle_key_counter = 0;
                    }
                }
            } else {
                self.toggle_key_counter = 0;
            }
        }
    }

    pub fn is_enabled(self: &Self) -> bool {
        self.enabled
    }
}
