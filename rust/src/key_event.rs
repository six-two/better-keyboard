use std::cmp::Ordering;

// Event types
//// Used as markers to separate events. Events may be separated in time or in space, such as with the multitouch protocol
pub const EV_SYN: i16 = 0x00;
//// Used to describe state changes of keyboards, buttons, or other key-like devices.
pub const EV_KEY: i16 = 0x01;

// EV_KEY values
//// When the key is released, an event is emitted with value 0
pub const EK_RELEASED: i32 = 0;
//// When a key is depressed, an event with the key’s code is emitted with value 1
pub const EK_PRESSED: i32 = 1;

const SECOND: i64 = 1000*1000;

#[derive(Debug, Clone)]
pub struct Timestamp {
    seconds: i64,
    micros: i64,
}

#[derive(Debug, Clone)]
pub struct KeyEvent {
    pub time: Timestamp,
    pub event_type: i16,
    pub code: i16,
    pub value: i32,
}

impl Timestamp {
    pub fn time_diff(a: &Timestamp, b: &Timestamp) -> f64 {
        let mut sec = a.seconds - b.seconds;
        let mut micros = a.micros - b.micros;
        if micros < SECOND {
            micros += SECOND;
            sec -= 1;
        }
        return sec as f64 + (micros as f64 / SECOND as f64);
    }

    pub fn new(seconds: i64, micros: i64) -> Result<Self, String> {
        if seconds < 0 {
            Err(format!("Timestamp: Invalid value for seconds: {}", seconds))
        } else if micros < 0 || micros >= SECOND {
            Err(format!("Timestamp: Invalid value for micros: {}", micros))
        } else {
            Ok(Timestamp { seconds, micros })
        }
    }

    pub fn get_seconds(self: &Self) -> i64 {
        self.seconds
    }

    pub fn get_micros(self: &Self) -> i64 {
        self.micros
    }

    pub fn add_millis(self: &mut Self, millis: i64) {
        self.micros += millis * 1000;
        while self.micros > SECOND {
            self.micros -= SECOND;
            self.seconds += 1;
        }
    }

    pub fn cmp(&self, other: &Self) -> Ordering {
        let time_diff: f64 = Timestamp::time_diff(self, other);
        if time_diff < 0_f64 {
            Ordering::Less
        } else if time_diff == 0_f64 {
            Ordering::Equal
        } else {
            Ordering::Greater
        }
    }    
}
