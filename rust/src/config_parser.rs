use crate::key_codes::KeyNameMap;
use serde::Deserialize;
use serde_yaml;
use std::collections::{HashMap, HashSet};

#[derive(Debug)]
pub struct Config {
    normal_hold: HashMap<i16, i16>,
    normal_type: HashMap<i16, Vec<i16>>,
    magic_hold: HashMap<i16, i16>,
    magic_type: HashMap<i16, Vec<i16>>,
    ignore_keys: HashSet<i16>,
    toggle_enabled_key: i16,
}

#[derive(Deserialize, Debug)]
pub struct ConfigFile {
    normal_hold: HashMap<String, String>,
    normal_type: HashMap<String, Vec<String>>,
    magic_hold: HashMap<String, String>,
    magic_type: HashMap<String, Vec<String>>,
    ignore_keys: HashSet<String>,
    toggle_enabled_key: String,
}

impl Config {
    pub fn map_key_hold(self: &Self, code: i16, magic_pressed: bool) -> i16 {
        let mut mapped_code = None;
        // first try to get the magic map
        if magic_pressed {
            mapped_code = self.magic_hold.get(&code);
        }
        // if none exists, fall back to default
        if mapped_code == None {
            mapped_code = self.normal_hold.get(&code);
        }

        match mapped_code {
            // return the mapped code if it exists
            Some(x) => *x,
            // otherwise just return the original value
            None => code,
        }
    }

    pub fn map_key_type(self: &Self, code: i16, magic_pressed: bool) -> Vec<i16> {
        let mut mapped_code = None;
        // first try to get the magic map
        if magic_pressed {
            mapped_code = self.magic_type.get(&code);
        }
        // if none exists, fall back to default
        if mapped_code == None {
            mapped_code = self.normal_type.get(&code);
        }

        match mapped_code {
            // return the mapped code if it exists
            Some(x) => x.clone(),
            // otherwise just return the original value
            None => vec![code],
        }
    }

    pub fn is_key_ignored(self: &Self, code: i16) -> bool {
        self.ignore_keys.contains(&code)
    }

    pub fn is_toggle_enabled_key(self: &Self, code: i16) -> bool {
        self.toggle_enabled_key == code
    }
}

pub fn parse_file(path: &str, key_name_map: &KeyNameMap) -> Result<Config, String> {
    let file = std::fs::File::open(path).map_err(|x| format!("{:?}", x))?;
    let config: ConfigFile = serde_yaml::from_reader(file).map_err(|x| format!("{:?}", x))?;

    convert_key_names_to_codes(&config, key_name_map)
}

fn convert_map_hold(map_hold: &HashMap<String, String>, key_name_map: &KeyNameMap) -> Result<HashMap<i16, i16>, String> {
    let mut map: HashMap<i16, i16> = HashMap::new();
    for (key, value) in map_hold.iter() {
        let key_int = key_name_map.get_key_code(key)?;
        let value_int = key_name_map.get_key_code(value)?;
        map.insert(key_int, value_int);
    }
    Ok(map)
}

fn convert_map_type(map_type: &HashMap<String, Vec<String>>, key_name_map: &KeyNameMap) -> Result<HashMap<i16, Vec<i16>>, String> {
    let mut map: HashMap<i16, Vec<i16>> = HashMap::new();
    for (key, value_list) in map_type.iter() {
        let key_int = key_name_map.get_key_code(key)?;
        let mut value_int_list: Vec<i16> = Vec::with_capacity(value_list.len());
        for value in value_list.iter() {
            let value_int = key_name_map.get_key_code(value)?;
            value_int_list.push(value_int);
        }
        map.insert(key_int, value_int_list);
    }
    Ok(map)
}

fn convert_key_names_to_codes(
    config_file: &ConfigFile,
    key_name_map: &KeyNameMap,
) -> Result<Config, String> {
    eprintln!("Config with names: {:?}", config_file);

    let normal_hold = convert_map_hold(&config_file.normal_hold, &key_name_map)?;
    let normal_type = convert_map_type(&config_file.normal_type, &key_name_map)?;
    let magic_hold = convert_map_hold(&config_file.magic_hold, &key_name_map)?;
    let magic_type = convert_map_type(&config_file.magic_type, &key_name_map)?;
    let toggle_enabled_key = key_name_map.get_key_code(&config_file.toggle_enabled_key)?;

    let mut ignore_keys: HashSet<i16> = HashSet::new();
    for key in config_file.ignore_keys.iter() {
        let key_int = key_name_map.get_key_code(key)?;
        ignore_keys.insert(key_int);
    }

    let config = Config {
        normal_hold,
        normal_type,
        magic_hold,
        magic_type,
        ignore_keys,
        toggle_enabled_key,
    };
    eprintln!("Config with codes: {:?}", config);
    Ok(config)
}
